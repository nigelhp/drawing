package com.nigelhp.drawing.render


private [render] object Border {
  private val VerticalEdge = "|"
  private val HorizontalEdge = "-"

  def surround(image: Image): Image = {
    val imageWidth = image.headOption.map(_.length).getOrElse(0)
    val horizontalBorder = horizontalBorderFor(imageWidth)
    horizontalBorder +: image.map(addVerticalBorders) :+ horizontalBorder
  }

  private def horizontalBorderFor(imageWidth: Int): String = {
    val borderWidth = imageWidth + (VerticalEdge.length * 2)
    HorizontalEdge * borderWidth
  }

  private def addVerticalBorders(content: String): String =
    content.mkString(start = VerticalEdge, sep = "", end = VerticalEdge)
}
