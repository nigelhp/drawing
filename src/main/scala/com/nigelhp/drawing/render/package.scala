package com.nigelhp.drawing


package object render {
  /*
   * We assume an image has a uniform width.
   */
  type Image = Seq[String]
}
