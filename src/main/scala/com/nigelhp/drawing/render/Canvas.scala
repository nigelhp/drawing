package com.nigelhp.drawing.render


import com.nigelhp.drawing.render.Shapes.Filler

private [render] object Canvas {
  private val Paper = ' '
  private val Pen = 'x'

  def draw(width: Int, height: Int)(fillers: Seq[Filler]): Image = {
    val canvas = emptyCanvas(width, height)
    for {
      y <- 1 to height
      x <- 1 to width
    } if (fillers.exists(_(x, y))) canvas(y - 1)(x - 1) = Pen
    canvas.map(_.mkString).toList
  }

  private def emptyCanvas(width: Int, height: Int): Array[Array[Char]] =
    Array.fill(n = height)(elem = emptyRow(width))

  private def emptyRow(width: Int): Array[Char] =
    Array.fill(n = width)(elem = Paper)
}
