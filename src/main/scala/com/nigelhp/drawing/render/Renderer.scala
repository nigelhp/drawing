package com.nigelhp.drawing.render


import com.nigelhp.drawing.domain.Drawing

object Renderer {
  def render(drawing: Drawing): Image = {
    val fillers = drawing.shapes.map(Shapes.fillerFor)
    val image = Canvas.draw(drawing.width, drawing.height)(fillers)
    Border.surround(image)
  }
}
