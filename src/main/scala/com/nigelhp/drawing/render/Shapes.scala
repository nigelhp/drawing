package com.nigelhp.drawing.render


import com.nigelhp.drawing.domain.{Line, Rectangle, Shape}

private [render] object Shapes {
  type Filler = (Int, Int) => Boolean

  def fillerFor(shape: Shape): Filler =
    shape match {
      case Line(x1, y1, x2, y2) if y1 == y2 =>                 // horizontal
        (x, y) => y == y1 && x >= x1 && x <= x2
      case Line(x1, y1, x2, y2) if x1 == x2 =>                 // vertical
        (x, y) => x == x1 && y >= y1 && y <= y2
      case Rectangle(x1, y1, x2, y2) =>
        (x, y) =>
          ((y == y1 || y == y2) && x >= x1 && x <= x2) ||
            ((x == x1 || x == x2) && y >= y1 && y <= y2)
    }
}
