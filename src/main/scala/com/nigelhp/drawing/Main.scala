package com.nigelhp.drawing


import com.nigelhp.drawing.command.{Command, Parser, Quit}
import com.nigelhp.drawing.domain.{Artist, Drawing}
import com.nigelhp.drawing.render.{Image, Renderer}

import scala.io.StdIn

object Main {
  type Executor[A] = (Option[A], Command) => Option[A]

  // NOTE: recursive function
  def run[A](executor: Executor[A])(commands: Stream[Command], state: Option[A] = None): Stream[Option[A]] =
    commands.takeWhile(_ != Quit).headOption.fold(Stream.empty[Option[A]]) { cmd =>
      val newState = executor(state, cmd)
      newState #:: run(executor)(commands.tail, newState)
    }

  def main(args: Array[String]): Unit =
    run(Artist.draw)(commands()).flatten.foreach(display)

  private def commands(): Stream[Command] =
    userInput().flatMap(Parser.parse)

  // NOTE: recursive function
  private def userInput(): Stream[String] =
    StdIn.readLine("enter command: ") #:: userInput()

  private val display: Drawing => Unit =
    (Renderer.render _).andThen(print)

  private def print(image: Image): Unit = {
    println()
    image.foreach(println)
  }
}