package com.nigelhp.drawing.domain


import com.nigelhp.drawing.command.{Command, DrawLine, DrawRectangle, NewCanvas, Quit}

object Artist {
  def draw(maybeDrawing: Option[Drawing], command: Command): Option[Drawing] =
    command match {
      case NewCanvas(width, height) =>
        Some(Drawing(width, height, shapes = Seq.empty))
      case DrawLine(x1, y1, x2, y2) =>
        drawShapeOn(maybeDrawing, Line(x1, y1, x2, y2))
      case DrawRectangle(x1, y1, x2, y2) =>
        drawShapeOn(maybeDrawing, Rectangle(x1, y1, x2, y2))
      case Quit =>
        maybeDrawing
    }

  private def drawShapeOn(maybeDrawing: Option[Drawing], shape: => Shape): Option[Drawing] =
    maybeDrawing.map { drawing =>
      drawing.copy(shapes = shape +: drawing.shapes)
    }
}
