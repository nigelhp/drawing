package com.nigelhp.drawing.domain


sealed trait Shape
final case class Line private (x1: Int, y1: Int, x2: Int, y2: Int) extends Shape
final case class Rectangle private (x1: Int, y1: Int, x2: Int, y2: Int) extends Shape

final case class Drawing(width: Int, height: Int, shapes: Seq[Shape])

object Line {
  def apply(x1: Int, y1: Int, x2: Int, y2: Int): Line = {
    require(x1 == x2 || y1 == y2, "Line must be horizontal or vertical")
    // we can safely reorganise:
    // - a vertical line to be top->bottom
    // - a horizontal line to be left->right
    if (x1 == x2) new Line(x1, y1.min(y2), x1, y1.max(y2))
    else new Line(x1.min(x2), y1, x1.max(x2), y1)
  }
}

object Rectangle {
  def apply(x1: Int, y1: Int, x2: Int, y2: Int): Rectangle = {
    require(x2 > x1 && y2 > y1, "Rectangle must be (upper-left, lower-right)")
    new Rectangle(x1, y1, x2, y2)
  }
}