package com.nigelhp.drawing.command


sealed trait Command
final case class NewCanvas(width: Int, height: Int) extends Command
final case class DrawLine(x1: Int, y1: Int, x2: Int, y2: Int) extends Command
final case class DrawRectangle(x1: Int, y1: Int, x2: Int, y2: Int) extends Command
case object Quit extends Command
