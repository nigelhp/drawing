package com.nigelhp.drawing.command


import scala.util.{Success, Try}

object Parser {
  private val NewCanvasPrefix = "C "
  private val DrawLinePrefix = "L "
  private val DrawRectanglePrefix = "R "

  def parse(input: String): Option[Command] =
    input match {
      case _ if input.startsWith(NewCanvasPrefix) =>
        val args = toNumericArguments(input.drop(NewCanvasPrefix.length))
        maybeNewCanvas.lift(args)
      case _ if input.startsWith(DrawLinePrefix) =>
        val args = toNumericArguments(input.drop(DrawLinePrefix.length))
        maybeDrawLine.lift(args)
      case _ if input.startsWith(DrawRectanglePrefix) =>
        val args = toNumericArguments(input.drop(DrawRectanglePrefix.length))
        maybeDrawRectangle.lift(args)
      case "Q" => Some(Quit)
      case _ => None
    }

  private def maybeNewCanvas: PartialFunction[List[Try[Int]], NewCanvas] = {
    case Success(width) :: Success(height) :: Nil =>
      NewCanvas(width, height)
  }

  private def maybeDrawLine: PartialFunction[List[Try[Int]], DrawLine] = {
    case Success(x1) :: Success(y1) :: Success(x2) :: Success(y2) :: Nil if x1 == x2 || y1 == y2 =>
      DrawLine(x1, y1, x2, y2)
  }

  private def maybeDrawRectangle: PartialFunction[List[Try[Int]], DrawRectangle] = {
    case Success(x1) :: Success(y1) :: Success(x2) :: Success(y2) :: Nil if x2 > x1 && y2 > y1 =>
      DrawRectangle(x1, y1, x2, y2)
  }

  private def toNumericArguments(input: String): List[Try[Int]] =
    input.split(' ').map { str =>
      Try(str.toInt).filter(_ > 0)
    }.toList
}
