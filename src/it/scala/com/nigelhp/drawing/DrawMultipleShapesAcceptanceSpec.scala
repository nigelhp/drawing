package com.nigelhp.drawing


import com.nigelhp.drawing.DrawMultipleShapesAcceptanceSpec.ImageHeight
import com.nigelhp.drawing.support.AcceptanceSpec
import com.nigelhp.drawing.support.MainRunner.{drawLine, drawRectangle, newCanvas, runCommandsThenQuit}

class DrawMultipleShapesAcceptanceSpec extends AcceptanceSpec {

  feature("Draw multiple shapes on the canvas") {
    info("Lines are drawn with the 'x' character")

    scenario("Draw a horizontal line, a vertical line, and a rectangle") {
      Given("A new canvas of width 20 and height 5")
      val createCanvas = newCanvas(width = 20, height = 5)

      When("a horizontal line, a vertical line, and a rectangle are requested")
      whenReady(runCommandsThenQuit(
        createCanvas,
        drawLine(x1 = 1, y1 = 3, x2 = 7, y2 = 3),
        drawLine(x1 = 7, y1 = 1, x2 = 7, y2 = 3),
        drawRectangle(x1 = 15, y1 = 2, x2 = 20, y2 = 5)
      )) { output =>

        Then("the canvas should contain a horizontal line, a vertical line, and a rectangle")
        output.takeRight(ImageHeight) should contain theSameElementsInOrderAs Seq(
          "----------------------",
          "|      x             |",
          "|      x       xxxxxx|",
          "|xxxxxxx       x    x|",
          "|              x    x|",
          "|              xxxxxx|",
          "----------------------"
        )
      }
    }
  }
}

private object DrawMultipleShapesAcceptanceSpec {
  val ImageHeight = 7
}