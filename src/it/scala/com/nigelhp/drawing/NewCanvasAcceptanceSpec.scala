package com.nigelhp.drawing


import com.nigelhp.drawing.support.AcceptanceSpec
import com.nigelhp.drawing.support.MainRunner.{newCanvas, runCommandsThenQuit}

class NewCanvasAcceptanceSpec extends AcceptanceSpec {

  feature("Create a new canvas") {
    info("A canvas has an exterior horizontal border consisting of the dash ('-') character")
    info("and an exterior vertical border consisting of the pipe ('|') character")

    scenario("with a valid width and height") {
      When("a new canvas is requested of width 20 and height 5")
      whenReady(runCommandsThenQuit(newCanvas(width = 20, height = 5))) { drawing =>

        Then("an empty canvas with interior dimensions of 20x5 is drawn surrounded by a border")
        drawing should contain theSameElementsInOrderAs Seq(
          "----------------------",
          "|                    |",
          "|                    |",
          "|                    |",
          "|                    |",
          "|                    |",
          "----------------------"
        )
      }
    }
  }
}
