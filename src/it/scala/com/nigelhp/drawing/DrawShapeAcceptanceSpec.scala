package com.nigelhp.drawing


import com.nigelhp.drawing.DrawShapeAcceptanceSpec.ImageHeight
import com.nigelhp.drawing.support.AcceptanceSpec
import com.nigelhp.drawing.support.MainRunner.{drawLine, drawRectangle, newCanvas, runCommandsThenQuit}

class DrawShapeAcceptanceSpec extends AcceptanceSpec {

  feature("Draw a single shape on the canvas") {
    info("Lines are drawn with the 'x' character")

    scenario("Draw a horizontal line") {
      Given("A new canvas of width 20 and height 5")
      val createCanvas = newCanvas(width = 20, height = 5)

      When("a line from (1,3) to (7,3) is requested")
      whenReady(runCommandsThenQuit(createCanvas, drawLine(x1 = 1, y1 = 3, x2 = 7, y2 = 3))) { output =>

        Then("the canvas should contain a horizontal line from (1,3) to (7,3)")
        output.takeRight(ImageHeight) should contain theSameElementsInOrderAs Seq(
          "----------------------",
          "|                    |",
          "|                    |",
          "|xxxxxxx             |",
          "|                    |",
          "|                    |",
          "----------------------"
        )
      }
    }

    scenario("Draw a vertical line") {
      Given("A new canvas of width 20 and height 5")
      val createCanvas = newCanvas(width = 20, height = 5)

      When("a line from (7,1) to (7,3) is requested")
      whenReady(runCommandsThenQuit(createCanvas, drawLine(x1 = 7, y1 = 1, x2 = 7, y2 = 3))) { output =>

        Then("the canvas should contain a vertical line from (7,1) to (7,3)")
        output.takeRight(ImageHeight) should contain theSameElementsInOrderAs Seq(
          "----------------------",
          "|      x             |",
          "|      x             |",
          "|      x             |",
          "|                    |",
          "|                    |",
          "----------------------"
        )
      }
    }

    scenario("Draw a rectangle") {
      Given("A new canvas of width 20 and height 5")
      val createCanvas = newCanvas(width = 20, height = 5)

      When("a rectangle with an upper left corner at (15,2) and a bottom right corner at (20,5) is requested")
      whenReady(runCommandsThenQuit(createCanvas, drawRectangle(x1 = 15, y1 = 2, x2 = 20, y2 = 5))) { output =>

        Then("the canvas should contain a rectangle between (15,2) and (20,5)")
        output.takeRight(ImageHeight) should contain theSameElementsInOrderAs Seq(
          "----------------------",
          "|                    |",
          "|              xxxxxx|",
          "|              x    x|",
          "|              x    x|",
          "|              xxxxxx|",
          "----------------------"
        )
      }
    }
  }
}

private object DrawShapeAcceptanceSpec {
  val ImageHeight = 7
}