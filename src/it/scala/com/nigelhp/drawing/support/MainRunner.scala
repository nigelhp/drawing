package com.nigelhp.drawing.support


import java.io.{ByteArrayOutputStream, StringReader}

import com.nigelhp.drawing.Main

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}
import scala.io.Source

object MainRunner {
  private val Prompt = "enter command: "
  private val Quit = "Q"

  def newCanvas(width: Int, height: Int): String =
    s"C $width $height"

  def drawLine(x1: Int, y1: Int, x2: Int, y2: Int): String =
    s"L $x1 $y1 $x2 $y2"

  def drawRectangle(x1: Int, y1: Int, x2: Int, y2: Int): String =
    s"R $x1 $y1 $x2 $y2"

  def runCommandsThenQuit(commands: String*): Future[Seq[String]] =
    run(commands :+ Quit).map(toLinesExcludingPrompts)

  private def run(commands: Seq[String]): Future[Source] =
    Future {
      val out = new ByteArrayOutputStream()
      blocking {
        Console.withIn(new StringReader(commands.mkString(start = "", sep = "\n", end = "\n"))) {
          Console.withOut(out) {
            Main.main(Array.empty)
          }
        }
      }

      Source.fromBytes(out.toByteArray)
    }

  private def toLinesExcludingPrompts(source: Source): Seq[String] =
    source.getLines().filterNot(_ == Prompt).toSeq
}
