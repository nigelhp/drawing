package com.nigelhp.drawing.support


import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Span}
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

abstract class AcceptanceSpec extends FeatureSpec with GivenWhenThen with Matchers with ScalaFutures {
  implicit val defaultPatience = PatienceConfig(timeout = scaled(Span(250, Millis)), interval = scaled(Span(15, Millis)))
}
