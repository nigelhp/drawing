package com.nigelhp.drawing.command


import org.scalatest.{FreeSpec, Matchers}

class ParserSpec extends FreeSpec with Matchers {

  "A command Parser" - {
    "parses a Quit command" in {
      Parser.parse(input = "Q") shouldBe Some(Quit)
    }

    "parses a New Canvas command when width and height are positive integral values" in {
      Parser.parse(input = "C 20 5") shouldBe Some(NewCanvas(width = 20, height = 5))
    }

    "parses a Draw Line command" - {
      "when the coordinates define a horizontal line" - {
        "and are specified left->right" in {
          Parser.parse(input = "L 1 3 7 3") shouldBe Some(DrawLine(x1 = 1, y1 = 3, x2 = 7, y2 = 3))
        }

        "and are specified right->left" in {
          Parser.parse(input = "L 7 3 1 3") shouldBe Some(DrawLine(x1 = 7, y1 = 3, x2 = 1, y2 = 3))
        }
      }

      "when the coordinates define a vertical line" - {
        "and are specified top->bottom" in {
          Parser.parse(input = "L 7 1 7 3") shouldBe Some(DrawLine(x1 = 7, y1 = 1, x2 = 7, y2 = 3))
        }

        "and are specified bottom->top" in {
          Parser.parse(input = "L 7 3 7 1") shouldBe Some(DrawLine(x1 = 7, y1 = 3, x2 = 7, y2 = 1))
        }
      }
    }

    "parses a Draw Rectangle command when the coordinates define a valid (upper-left, lower-right)" in {
      Parser.parse(input = "R 15 2 20 5") shouldBe Some(DrawRectangle(x1 = 15, y1 = 2, x2 = 20, y2 = 5))
    }

    "ignores" - {
      "a malformed New Canvas command" - {
        "when there are too few arguments" in {
          Parser.parse(input = "C 20") shouldBe None
        }

        "when there are too many arguments" in {
          Parser.parse(input = "C 20 5 2") shouldBe None
        }

        "when width" - {
          "is non-numeric" in {
            Parser.parse(input = "C X 5") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "C 20.1 5") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "C -20 5") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "C 0 5") shouldBe None
          }
        }

        "when height" - {
          "is non-numeric" in {
            Parser.parse(input = "C 20 Y") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "C 20 5.3") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "C 20 -5") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "C 20 0") shouldBe None
          }
        }
      }

      "a malformed Draw Line command" - {
        "when there are too few arguments" in {
          Parser.parse(input = "L 1 3 7") shouldBe None
        }

        "when there are too many arguments" in {
          Parser.parse(input = "L 1 3 7 3 9") shouldBe None
        }

        "when x1" - {
          "is non-numeric" in {
            Parser.parse(input = "L X 3 7 3") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "L 1.23 3 7 3") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "L -1 3 7 3") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "L 0 3 7 3") shouldBe None
          }
        }

        "when y1" - {
          "is non-numeric" in {
            Parser.parse(input = "L 1 X 7 3") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "L 1 3.45 7 3") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "L 1 -3 7 3") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "L 1 0 7 3") shouldBe None
          }
        }

        "when x2" - {
          "is non-numeric" in {
            Parser.parse(input = "L 1 3 X 3") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "L 1 3 7.89 3") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "L 1 3 -7 3") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "L 1 3 0 3") shouldBe None
          }
        }

        "when y2" - {
          "is non-numeric" in {
            Parser.parse(input = "L 1 3 7 X") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "L 1 3 7 3.45") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "L 1 3 7 -3") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "L 1 3 7 0") shouldBe None
          }
        }

        "when the coordinates do not define either a horizontal or vertical line" in {
          Parser.parse(input = "L 1 3 7 5") shouldBe None
        }
      }

      "a malformed Draw Rectangle command" - {
        "when there are too few arguments" in {
          Parser.parse(input = "R 15 2 20") shouldBe None
        }

        "when there are too many arguments" in {
          Parser.parse(input = "R 15 2 20 5 7") shouldBe None
        }

        "when x1" - {
          "is non-numeric" in {
            Parser.parse(input = "R X 2 20 5") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "R 15.67 2 20 5") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "R -15 2 20 5") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "R 0 2 20 5") shouldBe None
          }
        }

        "when y1" - {
          "is non-numeric" in {
            Parser.parse(input = "R 15 X 20 5") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "R 15 2.34 20 5") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "R 15 -2 20 5") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "R 15 0 20 5") shouldBe None
          }
        }

        "when x2" - {
          "is non-numeric" in {
            Parser.parse(input = "R 15 2 X 5") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "R 15 2 20.1 5") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "R 15 2 -20 5") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "R 15 2 0 5") shouldBe None
          }
        }

        "when y2" - {
          "is non-numeric" in {
            Parser.parse(input = "R 15 2 20 X") shouldBe None
          }

          "is a non-integral value" in {
            Parser.parse(input = "R 15 2 20 5.6") shouldBe None
          }

          "is negative" in {
            Parser.parse(input = "R 15 2 20 -5") shouldBe None
          }

          "is zero" in {
            Parser.parse(input = "R 15 2 20 0") shouldBe None
          }
        }

        "when the 'lower-right' coordinate" - {
          "is to the left of the first" in {
            Parser.parse(input = "R 15 2 10 5") shouldBe None
          }

          "is above the first" in {
            Parser.parse(input = "R 15 2 20 1") shouldBe None
          }

          "has a matching" - {
            "x coordinate with the first" in {
              Parser.parse(input = "R 15 2 15 5") shouldBe None
            }

            "y coordinate with the first" in {
              Parser.parse(input = "R 15 2 20 2") shouldBe None
            }
          }
        }
      }

      "an unrecognised command" in {
        Parser.parse(input = "HELP") shouldBe None
      }
    }
  }
}
