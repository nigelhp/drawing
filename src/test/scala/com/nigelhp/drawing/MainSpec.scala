package com.nigelhp.drawing


import com.nigelhp.drawing.command.{Command, DrawLine, NewCanvas, Quit}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}

class MainSpec extends FreeSpec with Matchers with MockFactory {

  private trait Fixture {
    type State = String
    val executor = mockFunction[Option[State], Command, Option[State]]
  }

  "Main" - {
    "runs the application" - {
      "doing nothing when the first command received is the Quit command" in new Fixture {
        executor.expects(*, *).never()

        val states = Main.run(executor)(commands = Stream(Quit))

        states shouldBe empty
      }

      "executing commands until the first Quit command" in new Fixture {
        val canvasCommand = NewCanvas(width = 20, height = 5)
        val drawCommand = DrawLine(x1 = 1, y1 = 3, x2 = 7, y2 = 3)
        executor.expects(None, canvasCommand).returning(Some("A"))
        executor.expects(Some("A"), drawCommand).returning(Some("B"))

        val states = Main.run(executor)(commands = Stream(canvasCommand, drawCommand, Quit, canvasCommand))

        states should contain theSameElementsInOrderAs Seq(Some("A"), Some("B"))
      }
    }
  }
}
