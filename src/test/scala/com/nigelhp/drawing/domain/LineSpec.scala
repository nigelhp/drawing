package com.nigelhp.drawing.domain


import org.scalatest.{FreeSpec, Matchers}

class LineSpec extends FreeSpec with Matchers {

  "A Line" - {
    "has the same location" - {
      "when horizontal if the coordinates are left->right or right->left" in {
        Line(x1 = 1, y1 = 3, x2 = 7, y2 = 3) shouldBe Line(x1 = 7, y1 = 3, x2 = 1, y2 = 3)
      }

      "when vertical if the coordinates are top->bottom or bottom->top" in {
        Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3) shouldBe Line(x1 = 7, y1 = 3, x2 = 7, y2 = 1)
      }
    }

    "must be horizontal or vertical" in {
      an [IllegalArgumentException] shouldBe thrownBy {
        Line(x1 = 1, y1 = 1, x2 = 3, y2 = 3)
      }
    }
  }
}
