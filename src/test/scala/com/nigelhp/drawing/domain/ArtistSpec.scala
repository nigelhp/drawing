package com.nigelhp.drawing.domain


import com.nigelhp.drawing.command.{DrawLine, DrawRectangle, NewCanvas, Quit}
import org.scalatest.{FreeSpec, Matchers}

class ArtistSpec extends FreeSpec with Matchers {

  "An Artist" - {
    "can create a blank canvas" - {
      "when there is no existing canvas" in {
        val newDrawing = Artist.draw(maybeDrawing = None, command = NewCanvas(width = 10, height = 5))

        newDrawing shouldBe Some(Drawing(width = 10, height = 5, shapes = Seq.empty))
      }

      "when a canvas already exists" in {
        val currentDrawing = Drawing(width = 2, height = 2, shapes = Seq(Line(x1 = 1, y1 = 1, x2 = 2, y2 = 1)))

        val newDrawing = Artist.draw(maybeDrawing = Some(currentDrawing), command = NewCanvas(width = 10, height = 5))

        newDrawing shouldBe Some(Drawing(width = 10, height = 5, shapes = Seq.empty))
      }
    }

    "can draw onto a canvas" - {
      "a horizontal line" in {
        val canvas = Drawing(width = 10, height = 5, shapes = Seq(Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3)))

        val newDrawing = Artist.draw(Some(canvas), command = DrawLine(x1 = 1, y1 = 3, x2 = 7, y2 = 3))

        newDrawing shouldBe Some(Drawing(width = 10, height = 5, shapes = Seq(
          Line(x1 = 1, y1 = 3, x2 = 7, y2 = 3),
          Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3)
        )))
      }

      "a vertical line" in {
        val canvas = Drawing(width = 10, height = 5, shapes = Seq(Line(x1 = 1, y1 = 3, x2 = 7, y2 = 3)))

        val newDrawing = Artist.draw(Some(canvas), command = DrawLine(x1 = 7, y1 = 1, x2 = 7, y2 = 3))

        newDrawing shouldBe Some(Drawing(width = 10, height = 5, shapes = Seq(
          Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3),
          Line(x1 = 1, y1 = 3, x2 = 7, y2 = 3)
        )))
      }

      "a rectangle" in {
        val canvas = Drawing(width = 10, height = 5, shapes = Seq(Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3)))

        val newDrawing = Artist.draw(Some(canvas), command = DrawRectangle(x1 = 3, y1 = 2, x2 = 6, y2 = 4))

        newDrawing shouldBe Some(Drawing(width = 10, height = 5, shapes = Seq(
          Rectangle(x1 = 3, y1 = 2, x2 = 6, y2 = 4),
          Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3),
        )))
      }
    }

    "cannot draw" - {
      "a line when there is no canvas" in {
        Artist.draw(maybeDrawing = None, command = DrawLine(x1 = 1, y1 = 3, x2 = 7, y2 = 3)) shouldBe None
      }

      "a rectangle when there is no canvas" in {
        Artist.draw(maybeDrawing = None, command = DrawRectangle(x1 = 3, y1 = 2, x2 = 6, y2 = 4)) shouldBe None
      }
    }

    "returns the current drawing when asked to stop" in {
      val canvas = Drawing(width = 10, height = 5, shapes = Seq(Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3)))

      Artist.draw(Some(canvas), command = Quit) shouldBe Some(canvas)
    }
  }
}
