package com.nigelhp.drawing.render


import com.nigelhp.drawing.domain.{Drawing, Line, Rectangle}
import org.scalatest.{FreeSpec, Matchers}

class RendererSpec extends FreeSpec with Matchers {

  "A renderer" - {
    "produces an image from a drawing with a border" in {
      val drawing = Drawing(width = 20 , height = 5, shapes = Seq(
        Rectangle(x1 = 15, y1 = 2, x2 = 20, y2 = 5),
        Line(x1 = 7, y1 = 1, x2 = 7, y2 = 3),
        Line(x1 = 1, y1 = 3, x2 = 7, y2 = 3)
      ))

      Renderer.render(drawing) shouldBe Seq(
        "----------------------",
        "|      x             |",
        "|      x       xxxxxx|",
        "|xxxxxxx       x    x|",
        "|              x    x|",
        "|              xxxxxx|",
        "----------------------"
      )
    }
  }
}
