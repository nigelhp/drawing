package com.nigelhp.drawing.render

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FreeSpec, Matchers}


class CanvasSpec extends FreeSpec with Matchers with MockFactory {

  private trait Fixture {
    def heightOf(canvas: Image): Int =
      canvas.length

    def widthsOf(canvas: Image): Set[Int] =
      canvas.map(_.length).toSet

    def contentsOf(canvas: Image): Set[Char] =
      canvas.foldLeft(Set.empty[Char]) { (acc, row) =>
        acc ++ row.toSet
      }
  }

  "A Canvas" - {
    "can be created in a blank state" - {
      "with dimensions 1x1" in new Fixture {
        val canvas = Canvas.draw(width = 1, height = 1)(fillers = Seq.empty)

        heightOf(canvas) shouldBe 1
        widthsOf(canvas) shouldBe Set(1)
        contentsOf(canvas) shouldBe Set(' ')
      }

      "with dimensions 20x5" in new Fixture {
        val canvas = Canvas.draw(width = 20, height = 5)(fillers = Seq.empty)

        heightOf(canvas) shouldBe 5
        widthsOf(canvas) shouldBe Set(20)
        contentsOf(canvas) shouldBe Set(' ')
      }

      "when the filler does not fill any coordinate" in new Fixture {
        val filler = mockFunction[Int, Int, Boolean]
        filler.expects(1, 1).returning(false)
        filler.expects(2, 1).returning(false)
        filler.expects(1, 2).returning(false)
        filler.expects(2, 2).returning(false)

        val canvas = Canvas.draw(width = 2, height = 2)(fillers = Seq(filler))

        contentsOf(canvas) shouldBe Set(' ')
      }
    }

    "can be drawn upon" in {
      val filler1 = mockFunction[Int, Int, Boolean]
      filler1.expects(1, 1).returning(true)
      filler1.expects(2, 1).returning(false)
      filler1.expects(3, 1).returning(true)
      filler1.expects(1, 2).returning(false)
      filler1.expects(2, 2).returning(false)
      filler1.expects(3, 2).returning(false)
      val filler2 = mockFunction[Int, Int, Boolean]
      filler2.expects(2, 1).returning(false)
      filler2.expects(1, 2).returning(false)
      filler2.expects(2, 2).returning(true)
      filler2.expects(3, 2).returning(false)

      val canvas = Canvas.draw(width = 3, height = 2)(fillers = Seq(filler1, filler2))

      canvas shouldBe Seq(
        "x x",
        " x ",
      )
    }
  }
}
