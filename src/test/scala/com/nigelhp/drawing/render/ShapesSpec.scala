package com.nigelhp.drawing.render

import com.nigelhp.drawing.domain.{Line, Rectangle}
import org.scalatest.{FreeSpec, Matchers}


class ShapesSpec extends FreeSpec with Matchers {

  "A Filler" - {
    "for a horizontal line" - {
      val horizontalLineFiller = Shapes.fillerFor(Line(x1 = 2, y1 = 3, x2 = 4, y2 = 3))

      "fills between the x coordinates on the target row" in {
        horizontalLineFiller(2, 3) shouldBe true
        horizontalLineFiller(3, 3) shouldBe true
        horizontalLineFiller(4, 3) shouldBe true
      }

      "does not fill outside the x coordinates on the target row" in {
        horizontalLineFiller(1, 3) shouldBe false
        horizontalLineFiller(5, 3) shouldBe false
      }

      "does not fill outside the target row" in {
        horizontalLineFiller(2, 2) shouldBe false
        horizontalLineFiller(3, 4) shouldBe false
      }
    }

    "for a vertical line" - {
      val verticalLineFiller = Shapes.fillerFor(Line(x1 = 3, y1 = 2, x2 = 3, y2 = 4))

      "fills between the y coordinates on the target column" in {
        verticalLineFiller(3, 2) shouldBe true
        verticalLineFiller(3, 3) shouldBe true
        verticalLineFiller(3, 4) shouldBe true
      }

      "does not fill outside the y coordinates on the target column" in {
        verticalLineFiller(3, 1) shouldBe false
        verticalLineFiller(3, 5) shouldBe false
      }

      "does not fill outside the target column" in {
        verticalLineFiller(2, 2) shouldBe false
        verticalLineFiller(4, 3) shouldBe false
      }
    }

    "for a rectangle" - {
      val rectangleFiller = Shapes.fillerFor(Rectangle(x1 = 2, y1 = 1, x2 = 4, y2 = 3))

      "fills the horizontal lines between the x coordinates on the two edge rows" in {
        rectangleFiller(2, 1) shouldBe true
        rectangleFiller(3, 1) shouldBe true
        rectangleFiller(4, 1) shouldBe true
        rectangleFiller(2, 3) shouldBe true
        rectangleFiller(3, 3) shouldBe true
        rectangleFiller(4, 3) shouldBe true
      }

      "fills the vertical lines between the y coordinates on the two edge columns" in {
        rectangleFiller(2, 2) shouldBe true
        rectangleFiller(4, 2) shouldBe true
      }

      "does not fill the interior of the rectangle" in {
        rectangleFiller(3, 2) shouldBe false
      }

      "does not fill outside the rectangle" in {
        rectangleFiller(1, 1) shouldBe false
        rectangleFiller(5, 2) shouldBe false
        rectangleFiller(3, 4) shouldBe false
      }
    }
  }
}
