package com.nigelhp.drawing.render


import org.scalatest.{FreeSpec, Matchers}

class BorderSpec extends FreeSpec with Matchers {

  private trait Fixture {
    def canvas(withWidth: Int, withHeight: Int): Image = {
      val emptyRow = " " * withWidth
      Array.fill(withHeight)(emptyRow).toList
    }
  }

  "A Border" - {
    "can be drawn around a canvas" - {
      "when the canvas has dimensions of 1x1" in new Fixture {
        Border.surround(canvas(withWidth = 1, withHeight = 1)) shouldBe Seq(
          "---",
          "| |",
          "---"
        )
      }

      "when the canvas has dimensions of 20x5" in new Fixture {
        Border.surround(canvas(withWidth = 20, withHeight = 5)) shouldBe Seq(
          "----------------------",
          "|                    |",
          "|                    |",
          "|                    |",
          "|                    |",
          "|                    |",
          "----------------------"
        )
      }

      "when the canvas has dimensions of 2x4" in new Fixture {
        Border.surround(canvas(withWidth = 2, withHeight = 4)) shouldBe Seq(
          "----",
          "|  |",
          "|  |",
          "|  |",
          "|  |",
          "----"
        )
      }
    }
  }
}
