import Dependencies._
import sbtassembly.AssemblyPlugin.defaultUniversalScript

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.nigelhp"

lazy val root = (project in file(".")).
  configs(IntegrationTest).
  settings(
    name := "drawing",
    scalacOptions ++= Seq(
      "-target:jvm-1.8",
      "-encoding", "utf8",
      "-deprecation",
      "-feature",
      "-unchecked",
      "-Xcheckinit",
      "-Xlint:_",
      "-Yno-adapted-args",
      "-Ywarn-dead-code",
      "-Ywarn-value-discard",
      "-Ywarn-unused"
    ),
    mainClass in assembly := Some("com.nigelhp.drawing.Main"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(
      prependShellScript = Some(defaultUniversalScript(shebang = true))
    ),
    scapegoatVersion in ThisBuild := "1.3.8",
    Defaults.itSettings,
    libraryDependencies ++= Seq(
      scalaMock % Test,
      scalaTest % s"$IntegrationTest,$Test"
    )
  )
