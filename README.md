### Drawing Program


##### Design Decisions

For now, invalid commands are simply ignored.  This is the case if:
* an unrecognised command is input (such as 'QUIT' rather than 'Q')
* an unexpected number of arguments are supplied for a command
* argument values have an incorrect type or are otherwise inconsistent with the domain (such as a non-positive canvas 
width)
* requests to draw lines that are not horizontal or vertical

Commands are currently parsed strictly in accordance with the specification document, and so neither 'Q ' or 'q' match
the Quit command 'Q'.

A command to draw a line or rectangle when there is no current canvas is simply ignored.

Commands that define lines / rectangles with coordinates that fall outside the boundary of the current canvas are 
accepted, but only that part falling within the canvas will be rendered.

A horizontal line with coordinates x2 < x1 is accepted, as the intent is clear.  Likewise, a vertical line with
coordinates y2 < y1 is accepted.

The command to create a new canvas clears any existing drawing without prompting for confirmation.

There is clearly much that can be improved in the handling of user input and user feedback to make the experience more
user friendly.


### ToDo

Coordinates are currently represented by plain `Int` fields.  
There is an opportunity to represent the coordinate system as a domain concept.  The current implementation validates 
such values at the boundary of the system, so they can be safely used.  However, this requires the `Parser` to enforce 
domain constraints that arguably should not be its responsibility.  The advantage of the current approach for this
simple application is that any invalid values (from pure gibberish to domain violations) are handled in the same way -
the command is simply ignored. 


### Development Tasks

This project uses the [Simple Build Tool (sbt)](https://www.scala-sbt.org/).

Key tasks can be achieved as follows:


Run the acceptance tests:

`sbt [clean] it:test`

Run the unit tests:

`sbt [clean] test`

Determine unit test coverage:

`sbt clean coverage test coverageReport`

Run static analysis:

`sbt scapegoat`

Run the application:

`sbt run`

Create a standalone executable 'fat jar':

`sbt clean assembly`

If you are running on Windows you will need to rename the resulting file with a `.bat` extension for it to be
executable.  Further details can be found here: 
[prepending-a-launch-script](https://github.com/sbt/sbt-assembly#prepending-a-launch-script).


### Project Structure

The source code is organised as follows:

```
src
├── it
├── main
└── test
```

* it   - acceptance tests
* main - non-test application code
* test - unit tests
